package main

import (
	"fmt"
	"go-fiber-gorm/book"
	"go-fiber-gorm/database"

	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm"
	"gorm.io/driver/sqlite"
)

func helloWorld(c *fiber.Ctx) error{
	return c.SendString("Hello World")
}
func setupRoutes(app *fiber.App){
	app.Get("api/v1/book", book.GetBooks)
	app.Get("api/v1/book/:id", book.GetBook)
	app.Post("api/v1/book", book.NewBook)
	app.Delete("api/v1/book/:id", book.DeleteBooks)
}

func initDatabase(){
	var err error
	database.DBConn, err = gorm.Open(sqlite.Open("gorm.db"), &gorm.Config{})
	if err != nil {
		panic("Failed to connect to database")
	}
	fmt.Println("Database connection successfully opened")

	database.DBConn.AutoMigrate(&book.Book{})
	fmt.Println("Database Migrated")
}

func main(){
	app := fiber.New()
	initDatabase()
	// defer database.DBConn.Close()

	app.Get("/", helloWorld)
	setupRoutes(app)

	const GFG = "GeeksforGeeks"

	app.Listen(":3000")
}
