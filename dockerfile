FROM golang:alpine as build-env

LABEL maintainer="Fahmy Abida <fahmyabida@gmail.com>"

ARG SERVICE_NAME=test

RUN mkdir /app
ADD . /app/

#COPY manifest/ /app

WORKDIR /app
RUN go build -o ${SERVICE_NAME} .

FROM alpine
WORKDIR /app
COPY --from=build-env /app/${SERVICE_NAME}          /app/${SERVICE_NAME}

RUN apk add --no-cache tzdata
ENV TZ Asia/Jakarta

ENTRYPOINT ["/app/test"]
